#include <stdio.h>
#include <pthread.h>
#include <stdlib.h>
#include <semaphore.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <time.h>

#define PROB_O 50
#define DEFAULT_TOTAL_MOLECULAS 10

#define ATTR_OFF        0
#define ATTR_BOLD       1
#define ATTR_UNDERSCORE 4
#define ATTR_BLINK      5
#define ATTR_REVERSE    7
#define ATTR_CONCEALED  8

#define BLACK   0
#define RED     1
#define GREEN   2
#define YELLOW  3
#define BLUE    4
#define MAGENTA 5
#define CYAN    6
#define WHITE   7

#define ATOM_ATTR ATTR_BOLD
#define ATOM_BG   BLACK

#define H_FG WHITE
#define O_FG WHITE

#define BARRIER_ATTR ATTR_BOLD
#define BARRIER_FG   RED
#define BARRIER_BG   BLACK

#define MOLECULE_ATTR ATTR_BOLD
#define MOLECULE_FG   BLUE
#define MOLECULE_BG   BLACK


typedef struct node * linkedList;

struct node {
	pthread_t t;
	linkedList next;
};

pthread_mutex_t mutex;

pthread_mutex_t mutexScreen;

pthread_barrier_t barrier;

sem_t semOxy;
sem_t semHydro;

volatile int oxygen;
volatile int hydrogen;

int rows;
int cols;

/* Vetores que contém os status das colunas da animação,
 * tanto acima quanto abaixo da barreira, de modo que os 
 * elementos ou moléculas não se sobreponham. */
volatile char * atoms;
volatile char * molecules;

void resetColor() {
	printf("%c[0m", 0x1B);
}

void setColor(int attr, int fcolor, int bcolor) {
	printf("%c[%d;3%d;4%dm", 0x1B, attr, fcolor, bcolor);
}

void gotoxy(int x, int y) {
	printf("%c[%d;%df", 0x1B, y, x);
}

void printBarrier() {
	int i, j;
	system("clear");
	gotoxy(0, 0);
	setColor(ATOM_ATTR, 0, ATOM_BG);
	for(i = 0; i < rows/2; i++) {
		for(j = 0; j < cols; j++) {
			printf(" ");
		}
		printf("\n");
	}
	setColor(BARRIER_ATTR, BARRIER_FG, BARRIER_BG);
	for(i = 0; i < cols; i++) {
		printf("=");
	}
	setColor(MOLECULE_ATTR, MOLECULE_FG, MOLECULE_BG);
	for(i = rows/2; i < rows; i++) {
		printf("\n");
		for(j = 0; j < cols; j++) {
			printf(" ");
		}
	}
	resetColor();
	fflush(stdout);
}

/* 
 * Função que anima a criação de uma thread do tipo O ou H. Aleatoriamente
 * seleciona uma coluna não ocupada na tela e anima a descida do elemento
 * do topo da tela à barreira. Retorna a posição escolhida para a thread
 * que a chamou. Todas as escritas na tela são protegidas por um lock.
 */
int animateAtom(char atom) {
	int i;
	int positionChosen;

	do {
		positionChosen = abs(rand() % cols);
	} while(atoms[positionChosen] != 0);
	
	atoms[positionChosen] = atom;
	
	for(i = 0; i < rows/2-1; i++) {
		pthread_mutex_lock(&mutexScreen);
		if(atom == 'H') {
			setColor(ATOM_ATTR, H_FG, ATOM_BG);
		} else {
			setColor(ATOM_ATTR, O_FG, ATOM_BG);
		}
		gotoxy(positionChosen, i);
		putchar(atom);
		resetColor();
		fflush(stdout);
		pthread_mutex_unlock(&mutexScreen);
		usleep(75000);
		
		pthread_mutex_lock(&mutexScreen);
		setColor(ATOM_ATTR, 0, ATOM_BG);
		gotoxy(positionChosen, i);
		printf(" ");
		resetColor();
		pthread_mutex_unlock(&mutexScreen);
	}
	pthread_mutex_lock(&mutexScreen);
	if(atom == 'H') {
		setColor(ATOM_ATTR, H_FG, ATOM_BG);
	} else {
		setColor(ATOM_ATTR, O_FG, ATOM_BG);
	}
	gotoxy(positionChosen, rows/2-1);
	putchar(atom);
	resetColor();
	fflush(stdout);
	pthread_mutex_unlock(&mutexScreen);

	return positionChosen;
}

/* 
 * Função que anima a criação de uma molécula H2O. Aleatoriamente seleciona
 * uma coluna não ocupada na tela e anima a descida da moléculada barreira
 * ao fim da tela. Todas as escritas na tela são protegidas por um lock.
 */
void animateMolecule() {
	int i;
	int positionChosen;

	do {
		positionChosen = abs(rand()) % (cols/5);
	} while (molecules[positionChosen] != 0);

	molecules[positionChosen] = 1;

	positionChosen *= 5;

	for (i = (rows/2)+2; i <= rows+1; i++) {
		pthread_mutex_lock(&mutexScreen);
		setColor(MOLECULE_ATTR, MOLECULE_FG, MOLECULE_BG);
		if (i > (rows/2)+2) {
			gotoxy(positionChosen, i-1);
			printf("   ");
		}
		if (i <= rows) {
			gotoxy(positionChosen, i);
			printf("  0  ");
		}
		if (i < rows) {
			gotoxy(positionChosen, (i+1));
			printf(" / \\ ");
		}
		if (i < rows-1) {
			gotoxy(positionChosen, (i+2));
			printf("H   H");
		}
		resetColor();
		fflush(stdout);
		pthread_mutex_unlock(&mutexScreen);
		usleep(75000);
	}

	positionChosen /= 5;

	molecules[positionChosen] = 0;
}

/* 
 * Função que representa a ligação do elemento na posição especificada a uma 
 * molécula, apagando-o da barreira. Atualiza o status daquela coluna como vaga
 * Todas as escritas na tela são protegidas por um lock.
 */
void bound(int pos) {
	atoms[pos] = 0;
	pthread_mutex_lock(&mutexScreen);
	setColor(ATOM_ATTR, 0, ATOM_BG);
	gotoxy(pos, rows/2-1);
	putchar(' ');
	resetColor();
	fflush(stdout);
	pthread_mutex_unlock(&mutexScreen);
}

/* 
 * Thread do tipo Oxigênio.
 */
void * thr_oxygen(void *v) {
	int pos;

	pthread_mutex_lock(&mutex);

	pos = animateAtom('O');

	oxygen += 1;
	if (hydrogen >= 2) {
		sem_post(&semHydro);
		sem_post(&semHydro);
		hydrogen -= 2;
		sem_post(&semOxy);
		oxygen -= 1;
	} else {
		pthread_mutex_unlock(&mutex);
	}

	sem_wait(&semOxy);

	bound(pos);

	pthread_barrier_wait(&barrier);
	pthread_mutex_unlock(&mutex);

	animateMolecule();

	return NULL;
}

/* 
 * Thread do tipo Hidrogênio.
 */
void * thr_hydrogen(void *v) {
	int pos;

	pthread_mutex_lock(&mutex);

	pos = animateAtom('H');

	hydrogen += 1;
	if (hydrogen >= 2 && oxygen >= 1) {
		sem_post(&semHydro);
		sem_post(&semHydro);
		hydrogen -= 2;
		sem_post(&semOxy);
		oxygen -= 1;
	} else {
		pthread_mutex_unlock(&mutex);
	}

	sem_wait(&semHydro);
	bound(pos);

	pthread_barrier_wait(&barrier);

	return NULL;
}

int main(int argc, char** argv) {
	linkedList l;
	int qtdH, qtdO, totalH, totalO;

	struct winsize w;


	/* Verifica se foi passado um argumento 
	 * para quantas moléculas devem ser criadas.
	 * Se nada foi passado, usa o padrão. */
	if (argc > 1) {
		totalO = atoi(argv[1]);
	}
	else {
		totalO = DEFAULT_TOTAL_MOLECULAS;
	}
	totalH = 2*totalO;

	ioctl(STDOUT_FILENO, TIOCGWINSZ, &w);

	rows = w.ws_row;
	cols = w.ws_col;

	printBarrier();

	atoms = calloc(cols, sizeof(char));
	molecules = calloc(cols/5, sizeof(char));

	pthread_mutex_init(&mutex, NULL);

	pthread_mutex_init(&mutexScreen, NULL);

	oxygen = 0;
	hydrogen = 0;

	pthread_barrier_init(&barrier, NULL, 3);

	sem_init(&semOxy, 0, 0);

	sem_init(&semHydro, 0, 0);

	srand (time(NULL));

	l = NULL;
	qtdH = qtdO = 0;
	while(qtdO < totalO || qtdH < totalH) {
		int r;
		linkedList n;

		n = malloc(sizeof *n);
		n->next = l;

		r = rand() % 10000;
		if(qtdH >= totalH || (qtdO < totalO && r < PROB_O * 100)) {
			pthread_create(&(n->t), NULL, thr_oxygen, NULL);
			qtdO++;
		} else {
			pthread_create(&(n->t), NULL, thr_hydrogen, NULL);
			qtdH++;
		}

		l = n;
	}

	while(l != NULL) {
		linkedList next = l->next;
		pthread_join(l->t, NULL);
		free(l);
		l = next;
	}

	sem_destroy(&semHydro);

	sem_destroy(&semOxy);

	pthread_barrier_destroy(&barrier);

	pthread_mutex_destroy(&mutexScreen);

	pthread_mutex_destroy(&mutex);

	resetColor();

	gotoxy(rows, cols);
	printf("\n\n");

	return 0;
}
