Problema
O problema abordado é o da formação da molécula de água. Para cada 2 átomos de Hidrogênios e 1 átomo de Oxigênio criados, deve-se gerar uma molécula de H2O. Traduzindo o problema para threads, devemos ter threads tipo 1 e 2, que aguardam na barreira. No momento em que há duas threads do tipo 1 e uma thread do tipo 2 na barreira, estas threads são liberadas (molécula é criada).

Solução
Nosso programa cria variadas threads que executam os dois tipos de funções, uma para representar H e outra para representar O, e utiliza barreira para as funções criadas. Após criar um átomo, este é colocado em um barreira se não houver átomos suficientes na barreira para formar uma molécula. Caso haja átomos suficientes, atravessam a barreira para montar a molécula. No momento da execução da thread que representa H, são necessárias uma thread representando H e outra representando O aguardando na barreira para montar a molécula. No momento da execução da thread que representa O, são necessárias duas threads representando H aguardando na barreira para montar a molécula.

A animação desenha uma barreira, que cobre toda a tela horizontalmente, com caracteres "=" no centro da tela. Após isto, cria um átomo no topo da tela contendo a letra H ou O, e desce-o com animação até a barreira.

As funções de criação de átomo, bem como a animação de criação chamada por elas, são mutuamente exclusivas entre si. Ou seja, um átomo só pode ser criado se nenhum outro estiver sendo criado e animado até a barreira.

A função de criação da molécula nao é exclusiva, ou seja, pode ocorrer em paralelo com a função de criação de átomos. Ela é chamada logo após a função bound, na qual os átomos são retirados da barreira. Durante a criação da molécula, é feita também a animação que desenha dois H's e um O interligados representando-a, e faz este desenho descer até a parte inferior da tela. No final da animação, o desenho é apagado.

Instruções de execução

Para compilar, execute:
make compile
Para rodar, execute:
./proj <N>
Onde N é um parâmetro opcional que representa a quantidade total de moléculas criadas, sendo que o valor default é 10.
No código também há constantes que podem ser alteradas para mudar o funcionamento do programa:
PROB_O é um numero de 0 á 100 que define a probabilidade de ser criado O em relação à H
ATOM_ATTR define o ATTR do print dos átomos (setado por default como bold)
ATOM_BG define a cor de background dos átomos (setado por default como black)
H_FG define a cor de foreground dos átomos de H (setado por default como white)
O_FG define a cor de foreground dos átomos de H (setado por default como white)
BARRIER_ATTR define o ATTR do print da barreira (setado por default como bold)
BARRIER_FG define a cor de foreground dos átomos de H (setado por default como red)
BARRIER_BG define a cor de background dos átomos (setado por default como black)
MOLECULE_ATTR define o ATTR do print das moléculas (setado por default como bold)
MOLECULE_FG define a cor de foreground dos átomos de H (setado por default como blue)
MOLECULE_BG define a cor de foreground dos átomos de H (setado por default como black)